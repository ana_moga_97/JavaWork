/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package seriaf.poo.tema4;

import java.util.Timer;
import java.util.TimerTask;

/**
 *
 * @author Ana
 */
public class GameEngine {

    private final Timer referee;
    private final TimerTask refereeTask;
    private final int valToGuess;
    boolean isUp = false;

    public GameEngine(int valueToGuess, int secondsAvailable) {
        this.valToGuess = valueToGuess;
        this.referee = new Timer();
        this.refereeTask = new TimerTask() {
            @Override
            public void run() {
                System.out.println("Time is up! You lose.");
                isUp = true;
            }
        };
        this.referee.schedule(this.refereeTask, secondsAvailable * 1000);
    }

    GuessResult guess(int value) throws TimeoutException {
        if (isUp) {
            throw new TimeoutException();
        } else {
            return new GuessResult(valToGuess, value);
        }
    }

    void cancelTime() {
        refereeTask.cancel();
    }
}
