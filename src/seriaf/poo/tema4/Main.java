/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package seriaf.poo.tema4;

import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author Ana
 */
public class Main {

    @SuppressWarnings("empty-statement")
    public static void main(String arg[]) {
        int valueToGuess;
        Random rand = new Random();
        valueToGuess = rand.nextInt(100);
        boolean gameIsOver = false;
        GameEngine gameEngine = new GameEngine(valueToGuess, 10);
        while (!gameIsOver) {
            try {
                int guessUser;
                Scanner input = new Scanner(System.in);
                guessUser = input.nextInt();
                GuessResult var = gameEngine.guess(guessUser);
                if (var.isWon()) {
                    gameIsOver = true;
                    gameEngine.cancelTime();
                }
                System.out.println(var.toString());
            } catch (TimeoutException e) {
                System.err.println("Time is up! You lose.");
            }
        }
    }
}
