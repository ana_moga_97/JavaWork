/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package seriaf.poo.tema4;

/**
 *
 * @author Ana
 */
public class GuessResult {

    int guessUser;
    int valueToGuess;

    public GuessResult(int user, int valToGuess) {
        this.guessUser = user;
        this.valueToGuess = valToGuess;
    }

    public boolean isWon() {
        return guessUser == valueToGuess;
    }

    public String toString() {
        String a = null;
        if (guessUser == valueToGuess) {
            a = "Exactly, you WIN!";
        } else if (guessUser > valueToGuess) {
            a = "Try HIGHER!";
        } else if (guessUser < valueToGuess) {
            a = "Try LOWER!";
        }
        return a;
    }
}
