Realiza?i un program care s� implementeze jocul "Ghice?te num�rul" contra timp. Calculatorul va genera un num�r aleator �ntre 1 ?i 100 ?i juc�torul va avea 10 secunde s� ghiceasc� num�rul, altfel jocul e considerat pierdut. La fiecare �ncercare, calculatorul va informa juc�torul dac� num�rul introdus este mai mic sau mai mare dec�t cel c�utat.

Cerin?e
Implementa?i o clas� GuessResult care s� aib� dou� metode publice:
boolean isWon() - �ntoarce true dac� num�rul a fost ghicit.
String toString() - �ntoarce un String care va fi prezentat juc�torului drept feedback la num�rul introdus: "Try LOWER!", "Try HIGHER!", "Exactly, you WIN!".
Implementa?i o clas� de tip excep?ie numit� TimeoutException
Implementa?i o clas� numit� GameEngine care s� aib� urm�toarele metode:
Un constructor GameEngine(int valueToGuess, int secondsAvailable) ale c�rui argumente reprezint� valoarea ce trebuie ghicit� ?i num�rul de secunde disponibile pentru a ghici num�rul;
GuessResult guess(int value) throws TimeoutException - o metod� apelat� de fiecare dat� c�nd juc�torul introduce un num�r; metoda va arunca excep?ie dac� s-a dep�?it timpul, altfel va �ntoarce un obiect de tip GuessResult care con?ine informa?iile legate de mutarea anterioar�.
Implementa?i o clas� Main, executabil�, care s� genereze un num�r aleator, s� instan?ieze un GameEngine ?i apoi s� citeasc� numere de la tastatur� d�nd feedback juc�torului p�n� la ghicirea num�rului sau p�n� la expirarea timpului.
Dac� timpul expir�, se va afi?a �n consol� imediat (nu la primul num�r introdus) textul "Time is up! You lose."
Observa?ii
Toate clasele trebuie s� fac� parte din pachetul seriaf.poo.tema4
Timpul de 10 secunde �ncepe s� curg� de la insta?ierea obiectului de tip GameEngine.